#ifndef COLA_H
#define COLA_H
#include "Puntero.h"

template<class T>
class Cola{
public:
	Cola(){
		root = NULL;
	}
	~Cola(){
		root = NULL;
	}
	Cola(T &t){
		root = new nodoCola(t);
	}
	Cola(const Cola &c){
		root = c.root;
	}
	void insEnd(T &t){
		if (!root){
			root = new nodoCola(t);
		}
		else{
			Puntero<nodoCola> aux = root;
			while (aux->sig){
				aux = aux->sig;
			}
			aux->sig = new nodoCola(t);
		}
	}

	bool Pertenece(T &t){
		return PerteneceRec(t, root);
	}

	T Frente(){
		return root->dato;
	}
	void extFst(){
		root = root->sig;
	}
	bool esVacia(){
		return root == NULL;
	}
	Cola& operator=(const Cola& c) {
		root->dato = c.root->dato;
		root->sig = c.root->sig;
	};
	bool operator==(const Cola& c) const{
		return root->dato == c.root->dato;
	}
	friend ostream& operator<<(ostream& out, const Cola& a){
		Puntero<nodoCola> aux = a.root;
		out << "{";
		while (aux){
			out << aux->dato<<" ";
			aux = aux->sig;
		}
		out << "}";
		return out;
	}
private:
	struct nodoCola{
		Puntero<nodoCola> sig;
		T dato;
		nodoCola(){
			dato = NULL;
			sig = NULL;
		}
		nodoCola(T d){
			dato = d;
			sig = NULL;
		}
	};
	bool PerteneceRec(T &t, Puntero<nodoCola> &cola){
		if (!cola) return false;
		else {
			if (cola->dato == t) return true;
			else return PerteneceRec(t, cola->sig);
		}
	}
	
	Puntero<nodoCola> root;

};
#endif