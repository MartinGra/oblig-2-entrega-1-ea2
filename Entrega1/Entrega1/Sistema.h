﻿#ifndef SISTEMA_H
#define SISTEMA_H

#include "ISistema.h"
#include "Cola.h"

class Sistema : public ISistema
{
public:
	Sistema();

	// Operación 1
	Tupla<TipoRetorno, Iterador<Tupla<nat, nat>>> EnlacesCriticos(Iterador<nat> computadoras, Iterador<Tupla<nat, nat>> enlaces) override;

	// Operación 2
	Tupla<TipoRetorno, Iterador<Tupla<nat, nat>>> AhorroEnlaces(Matriz<nat> & laboratorio) override;

	// Operación 3
	Tupla<TipoRetorno, Iterador<Cadena>> GerenteProyecto(Iterador<Cadena> tareas, Iterador<Tupla<Cadena, Cadena>> precedencias) override;

	Tupla<nat, nat, Array<Tupla<nat,nat>>> kruskal(Matriz<nat> adyacencia);
	nat indTarea(Cadena t, Array<Cadena> tareas);
	nat indicePC(nat c, Array<nat> computadoras);
	Matriz<nat> floydWarshall(Matriz<nat> mtx);
	void copiarMatriz(Matriz<nat> copia, Matriz<nat> original);


private:
};

#endif
