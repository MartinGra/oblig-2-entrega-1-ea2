﻿#include "CasoDePrueba.h"
#include "PruebaMemoria.h"
#include "ConductorPrueba.h"
#include "Sistema.h"
#include <iomanip>

Puntero<ISistema> Inicializar()
{
	return new Sistema();
}

void main()
{
	clock_t startTime = clock();
	
	Puntero<ConductorPrueba> cp = new ConductorPrueba();
	Array<Puntero<Prueba>> pruebas = Array<Puntero<Prueba>>(3);
	pruebas[0] = new PruebaMemoria();
	pruebas[1] = new CasoDePrueba(Inicializar);
	pruebas[2] = pruebas[0];
	cp->CorrerPruebas(pruebas.ObtenerIterador());
	
	cout << double(clock() - startTime) / (double)CLOCKS_PER_SEC << " seconds." << endl;
	int dato; cin >> dato;
	//	system("pause");
}
