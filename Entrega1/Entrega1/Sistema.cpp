﻿#include "Sistema.h"
#define REP(k,a) for(nat k=0; k < (a); k++)
Sistema::Sistema()
{
}

// Operación 1
Tupla<TipoRetorno, Iterador<Tupla<nat, nat>>> Sistema::EnlacesCriticos(Iterador<nat> computadoras, Iterador<Tupla<nat, nat>> enlaces)
{
	nat largo = 0;
	computadoras.Reiniciar();
	while (computadoras.HayElemento()){
		largo++;
		computadoras.Avanzar();
	}
	computadoras.Reiniciar();
	
	Matriz<nat> conexiones(largo);
	Array<nat> pcsIndexadas(largo);
	Puntero<Cola<Tupla<nat, nat>>> cc = new Cola<Tupla<nat, nat>>();
	Puntero<Cola<Tupla<nat, nat>>> conexionesACortar = new Cola<Tupla<nat, nat>>();
	nat contCC = 0;
	nat indexPcs = 0;

	while (computadoras.HayElemento()){
		pcsIndexadas[indexPcs++] = computadoras.ElementoActual();
		computadoras.Avanzar();
	}

	enlaces.Reiniciar();
	while (enlaces.HayElemento()){
		nat pcOrigen = enlaces.ElementoActual().ObtenerDato1();
		nat pcDestino = enlaces.ElementoActual().ObtenerDato2();
		nat indOri = indicePC(pcOrigen, pcsIndexadas);
		nat indDes = indicePC(pcDestino, pcsIndexadas);
		conexiones[indOri][indDes] = 1;
		conexiones[indDes][indOri] = 1;
		conexionesACortar->insEnd(TUPLA(indOri, indDes));
		enlaces.Avanzar();
	}
	enlaces.Reiniciar();

	Matriz<nat> aux(largo);
	Matriz<nat> paths;
	while (!conexionesACortar->esVacia()){
		copiarMatriz(aux, conexiones);
	
		Tupla<nat,nat> tupTemp = conexionesACortar->Frente();
		nat indOrigen = tupTemp.ObtenerDato1();
		nat indDestino = tupTemp.ObtenerDato2();
		
		aux[indOrigen][indDestino] = 999999999;
		paths = floydWarshall(aux);	
		if (paths[indOrigen][indDestino] == 999999999){
			contCC++;
			cc->insEnd(TUPLA(pcsIndexadas[indOrigen],pcsIndexadas[indDestino]));
		}
		conexionesACortar->extFst();
	}
	if (contCC > 0){
		nat in = 0;
		Array<Tupla<nat, nat>> cablesCriticos(contCC);
		while (!cc->esVacia()){
			cablesCriticos[in++] = cc->Frente();
			cc->extFst();
		}
		return Tupla<TipoRetorno, Iterador<Tupla<nat, nat>>>(OK, cablesCriticos.ObtenerIterador());
	}
	else{
		return Tupla<TipoRetorno, Iterador<Tupla<nat, nat>>>(OK, NULL);
	}
}

void Sistema::copiarMatriz(Matriz<nat> copia, Matriz<nat> original){
	nat cn = original.ObtenerLargo();
	REP(i, cn)
		REP(j, cn)
			copia[i][j] = original[i][j];
}

nat Sistema::indicePC(nat c, Array<nat> computadoras){
	//PRE : esta en el array
	REP(i, computadoras.ObtenerLargo())
		if (computadoras[i] == c) return i;
	return -1; // nunca va a llegar
}
Matriz<nat> Sistema::floydWarshall(Matriz<nat> mtrx){
	nat cn = mtrx.ObtenerLargo();
	Matriz<nat> minPaths(cn);
	REP(i,cn)
		REP(j, cn){
		if (i == j) minPaths[i][j] = 0;
		else if (mtrx[i][j] == 1) minPaths[i][j] = 1;
		else minPaths[i][j] = 999999999;
	}
		
	REP(k, cn)
		REP(i, cn)
			REP(j, cn){
				nat actual = minPaths[i][j];
				nat path1 = minPaths[i][k];
				nat path2 = minPaths[k][j];
				if (actual > path1 + path2) minPaths[i][j] = path1 + path2;
			}
	return minPaths;
}

// Operación 2
Tupla<TipoRetorno, Iterador<Tupla<nat, nat>>> Sistema::AhorroEnlaces(Matriz<nat> & laboratorio)
{
	Tupla<nat, nat, Array<Tupla<nat,nat>>> retKruskal = kruskal(laboratorio);
	nat largoMtx = laboratorio.ObtenerLargo();

	//Datos de Kruskal
	nat vConectados = retKruskal.ObtenerDato1();
	nat pesoTotal = retKruskal.ObtenerDato2();
	Array<Tupla<nat, nat>> arrRet = retKruskal.ObtenerDato3();
	
	nat contador = 0;
	REP(i, arrRet.ObtenerLargo()){
		if (arrRet[i].ObtenerDato1() < arrRet.ObtenerLargo()) contador++;
	} //Encuentro hasta donde esta cargado sin basura

	Array<Tupla<nat, nat>> arDeRango = Array<Tupla<nat, nat>>(contador);
	REP(i, contador){
		arDeRango[i] = arrRet[i];
	}

	if (vConectados == largoMtx){
		return Tupla<TipoRetorno, Iterador<Tupla<nat, nat>>>(OK, arDeRango.ObtenerIterador());
	}
	else{
		return Tupla<TipoRetorno, Iterador<Tupla<nat, nat>>>(ERROR, NULL);
	}
}

// Operación 3
Tupla<TipoRetorno, Iterador<Cadena>> Sistema::GerenteProyecto(Iterador<Cadena> tareas, Iterador<Tupla<Cadena, Cadena>> precedencias)
{	
	nat largo = 0;
	tareas.Reiniciar();//sino no corre la prueba 3.2
	while (tareas.HayElemento()){
		largo++;
		tareas.Avanzar();
	}
	tareas.Reiniciar();
	Array<Cadena> arrTareas = Array<Cadena>(largo);
	REP(i, arrTareas.ObtenerLargo()){
		arrTareas[i] = tareas.ElementoActual();
		tareas.Avanzar();
	} //obtengo array cargado con tareas por indice
	tareas.Reiniciar();
	
	Array<Tupla<int, Puntero<Cola<Cadena>>>> arrPrecedencias = Array<Tupla<int, Puntero<Cola<Cadena>>>>(largo);
	REP(i, arrPrecedencias.ObtenerLargo()){
		arrPrecedencias[i].ObtenerDato1() = 0;
		arrPrecedencias[i].ObtenerDato2() = new Cola<Cadena>();
	}
	
	Cadena orTemp, desTemp;
	precedencias.Reiniciar();//Porque las pruebas dan mal sino
	while (precedencias.HayElemento()){
		orTemp = precedencias.ElementoActual().ObtenerDato1();
		desTemp = precedencias.ElementoActual().ObtenerDato2();
		nat indOrigen = indTarea(orTemp, arrTareas); 
		nat indDestino = indTarea(desTemp, arrTareas);
		if (indOrigen != -1 && indDestino != -1){
		arrPrecedencias[indDestino].ObtenerDato1()++;//predecesores
		arrPrecedencias[indOrigen].ObtenerDato2()->insEnd(desTemp); //si no hago new cola<cadena>(), se cae por inicializarlo en null
		}
		precedencias.Avanzar();
	}
	precedencias.Reiniciar();
	
	Puntero<Cola<Cadena>> espera = new Cola<Cadena>();
	Puntero<Cola<Cadena>> ordenTopologico = new Cola<Cadena>();
	nat ordenT = 0;
	REP(i, arrPrecedencias.ObtenerLargo()){
		if (arrPrecedencias[i].ObtenerDato1() == 0){
			espera->insEnd(arrTareas[i]);
			ordenT++;
		}
	}
	
	if (ordenT > 0){
		while (!espera->esVacia()){
			Cadena vGradoCero = espera->Frente();
			nat indiceVertice = indTarea(vGradoCero, arrTareas);
			espera->extFst();
			ordenTopologico->insEnd(vGradoCero);
			Puntero<Cola<Cadena>> adyacentes = arrPrecedencias[indiceVertice].ObtenerDato2();
			
			while (!adyacentes->esVacia()){
				Cadena w = adyacentes->Frente();
				nat verticeAdy = indTarea(w,arrTareas);
				adyacentes->extFst();
				arrPrecedencias[verticeAdy].ObtenerDato1()--;
				if (arrPrecedencias[verticeAdy].ObtenerDato1() == 0){
					if (!espera->Pertenece(w) && !ordenTopologico->Pertenece(w))//Revisar si es necesario
					espera->insEnd(w);
				}
			}	
		}
		Array<Cadena> ordenTareas = Array<Cadena>(largo);
		REP(i, largo){
			ordenTareas[i] = ordenTopologico->Frente();
			ordenTopologico->extFst();
		}
		return Tupla<TipoRetorno, Iterador<Cadena>>(OK, ordenTareas.ObtenerIterador());
		
	} 
	return Tupla<TipoRetorno, Iterador<Cadena>>(ERROR, NULL);
	
}

//Retorna el indice de la tarea, o -1 si no esta
nat Sistema::indTarea(Cadena t, Array<Cadena> tareas){
	REP(i, tareas.ObtenerLargo())
		if (tareas[i] == t) return i;
	return -1; //Borrarlo, no puedo retornar -1 si es un nat
}

//Kruskal retorna una tupla que contiene la cantidad de vertices conectados, el peso total, y un array con tuplas (origen,destino) del recubrimiento minimo
Tupla<nat, nat, Array<Tupla<nat,nat>>> Sistema::kruskal(Matriz<nat> adyacencia){
	int cn = adyacencia.ObtenerLargo();
	int verticesConectados = 1;
	int pesoTotal = 0;
	Array<Array<int>> arbol = Array<Array<int>>(cn);
	
	Array<Tupla<nat, nat>> retornoOrDes = Array<Tupla<nat, nat>>(cn);
	int indice = 0;

	Array<int> pertenece = Array<int>(cn);
	for (int i = 0; i < cn; i++){
		arbol[i] = Array<int>(cn, 0);
		pertenece[i] = i;
	}
	int nodoA = 0;
	int nodoB = 0;
	int vertices = 1;
	while (vertices < cn){
		nat min = 999999999; //Mi infinito
		for (int i = 0; i < cn; i++)
			for (int j = 0; j < cn; j++)
				if (adyacencia[i][j])
					if (min > adyacencia[i][j] && adyacencia[i][j] != 0 && pertenece[i] != pertenece[j]){
						min = adyacencia[i][j];
						nodoA = i;
						nodoB = j;
					}
		if (pertenece[nodoA] != pertenece[nodoB]){
			verticesConectados++;
			pesoTotal += min;
			arbol[nodoA][nodoB] = min;
			arbol[nodoB][nodoA] = min;
			//Agrego :
			retornoOrDes[indice] = TUPLA(nodoA, nodoB);
			indice++;
			int temp = pertenece[nodoB];
			pertenece[nodoB] = pertenece[nodoA];
			for (int k = 0; k < cn; k++)
				if (pertenece[k] == temp)
					pertenece[k] = pertenece[nodoA];
		}
		vertices++;
	}
	Tupla<nat, nat, Array<Tupla<nat,nat>>> retorno = Tupla<nat, nat, Array<Tupla<nat,nat>>>(verticesConectados, pesoTotal, retornoOrDes);
	return retorno;
}