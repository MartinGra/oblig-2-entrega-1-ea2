Con este archivo van a poder visualizar el contenido de Cadenas, Arrays, Punteros y Tuplas de una forma m�s amigable cuando se encuentren debugueando estos tipos de datos.
Copiar el archivo Framework.natvis en el directorio:

Para VS2012
"%USERPROFILE%\My Documents\Visual Studio 2012\Visualizers\"

Para VS2013
"%USERPROFILE%\My Documents\Visual Studio 2013\Visualizers\"

Nota: El directorio de instalaci�n depende del usuario autenticado en el equipo. Si ya copiaron el archivo para un usuario, e intentan debuguear desde la sesi�n de otro (caso de las m�quinas de laboratorio), deben hacer la copia para el segundo usuario tambi�n.
